<?php

	class Familia extends Eloquent
	{
		protected $table = 'familias';
		protected $fillable = array("nombre");
		
		public function generos(){
	 		return $this->hasMany('Genero');
	 	}
	}