<!DOCTYPE HTML>
<html lang="es-ES">
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <div class="row">
            <!--errores de validación-->
            @if($errors->has())
                <div class="alert-box alert">          
                    @foreach ($errors->all('<p>:message</p>') as $message)
                        {{ $message }}
                    @endforeach
                    
                </div>
            @endif
 
            <table>
                {{ Form::open(array('url' => 'familia/update/'.$familia->id)) }}
                <tr>
                    <td>
                        {{ Form::label('nombre', 'Nombre') }}
                    </td>
                    <td>
                        {{ Form::text('nombre', Input::old('nombre') ? Input::old('nombre') : $familia->nombre) }}
                    </td>
                </tr>
 
                <tr>
                    <td>
 
                    </td>
                    <td>
                         {{ Form::submit('Actualizar familia') }}
                    </td>
                </tr>              
                {{ Form::close() }}
            </table>    
            
        </div>
    </body>
</html>