<?php

class GenerosController extends \BaseController {

    
    public function index()
    {
 
        $generos = Genero::all();
        return View::make("genero.index", array("generos" => $generos));
 
    }

    public function list_all()
    {
        $generos = Genero::orderBy('nombre')->get();

        $this->layout->nest('content', 'genero.all', array("generos" => $generos)); 
    }

     public function list_by_familia()
    {
        
        $familia_id = Input::get("id");

        $generos = DB::select("select * from generos where familia_id = $familia_id");


        $this->layout->nest('content', 'genero.all', array("generos" => $generos)); 
    }
 
    public function create()
    {

        //si es get
        if(Input::get())
        {
 
            $inputs = $this->getInputs(Input::all());
 
            if($this->validateForms($inputs) === true)
            {
 
                 $genero = new Genero($inputs);
 
                 if($genero->save())
                 {
 
                     return Redirect::to('genero/show')->with(array('mensaje' => 'El genero ha sido creado correctamente.'));
 
                 }
 
            }else{
 
                return Redirect::to('genero/create')->withErrors($this->validateForms($inputs))->withInput();
 
            }
 
        //si es post
        }else{
 
            return View::make("genero.create");
 
        }
 
    }
 
    public function update($id)
    {
 
        $genero = Genero::find($id);
        if(Input::get())
        {
 
            if($genero)
            {
 
                $inputs = $this->getInputs(Input::all());
 
                if($this->validateForms($inputs) === true)
                {
 
                    $genero->nombre = Input::get("nombre");
                    $genero->familia_id = Input::get("familia_id");
 
                     if($genero->save())
                     {
 
                         return Redirect::to('genero/show')->with(array('mensaje' => 'El genero se ha actualizado correctamente.'));
 
                     }
 
                }else{
 
                    return Redirect::to("genero/update/$id")->withErrors($this->validateForms($inputs))->withInput();
 
                }
 
            }else{
 
                return Redirect::to('genero/show')->with(array('mensaje' => 'El genero no existe.'));
                
            }
 
        }else{
 
            return View::make("genero.update", array("genero" => $genero));
            
        }
 
    }
 
    public function delete($id)
    {
 
        $genero = Genero::find($id);
        if($genero)
        {
 
            $genero->delete();
            return Redirect::to('genero/show')->with(array('mensaje' => 'El genero ha sido eliminada correctamente.'));
 
        }else{
 
            return Redirect::to('genero/show')->with(array('mensaje' => "El genero que intentas eliminar no existe."));
 
        }
 
    }
 
    //método privado para validar los formularios
    //reutilización de código
    private function validateForms($inputs = array())
    {
 
        $rules = array(
            'nombre'      => 'required',
            'familia_id'      => 'required|numeric'
        );
            
        $messages = array(
            'required'  => 'El campo :attribute es obligatorio.'
        );
    
        $validation = Validator::make($inputs, $rules, $messages);
 
        if($validation->fails())
        {
 
            return $validation;
 
        }else{
 
            return true;
 
        }
 
    }
 
  
    private function getInputs($inputs = array())
    {
 
        foreach($inputs as $key => $val)
        {
            $inputs[$key] = $val;
        }
        return $inputs;
    }

}
            