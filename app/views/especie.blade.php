@include('includes.navigation')

<div class="container">


<div class="cd-tabs">
    <nav>
        <ul class="cd-tabs-navigation">
            <li><a data-content="new" class="selected" href="#0">Información</a></li>
            <li><a data-content="gallery" href="#0">Galeria</a></li>
        </ul> <!-- cd-tabs-navigation -->
    </nav>

    <ul class="cd-tabs-content">
        <li data-content="new" class="selected">
           

            <div class="divPanel page-content">


                    <h1>{{{$especie->nombre_comun}}}</h1>
                    <hr>    

                    

        <div class="row-fluid">
            <!--Edit Main Content Area here-->
                <div class="span7" id="divMain">

                                
                    
            <div class="row-fluid">     
                <div class="span12">                           
                    <img src="/{{{$imagenes[0]->url}}}" class="img-polaroid imagen_principal" alt="">   </div>           
            </div>
            
            <hr>
            
            <div class="row-fluid">              
                <div class="span12">              
                    <p>{{{$especie->descripcion}}}</p>
                </div>       
            </div>
            
             
                </div>
                <!--End Main Content Area here-->
                
                <!--Edit Sidebar Content here-->
                <div class="span5 sidebar">

                    <div class="sidebox">
                        <h3 class="sidebox-title">{{{$especie->tipo}}}</h3>
                        <p>Nombre científico: {{{$especie->genero->nombre}}} {{{$especie->nombre_cientifico}}}</p>
                        <p>Familia: {{{$especie->genero->familia->nombre}}}</p>
                        

                        <p>Flores y Frutos: {{{$especie->frutos_flores}}}</p>
                   
                        <p>Tamaño:</p>

                        
                        <input type="hidden" id="min" value={{{$especie->distribucion_minima}}}>
                        <input type="hidden" id="max" value={{{$especie->distribucion_maxima}}}>

                    <div style="position: relative; padding: 10px;">

                        <div>
                            <input type="hidden" id="tamano" value={{{$especie->tamano}}}>
                        </div>

                    </div>
                    <br>
                    <br>
                        <p>Distribución altitudinal:</p>
                        
                   
                   <div style="position: relative; padding: 10px;">

                        <div>
                            <input type="text" id="range" value="" name="range" />
                        </div>

                    </div>
                            
                        
                            
                    </div>
                    
                </div>
                <!--End Sidebar Content here-->
            </div>

        <div id="footerInnerSeparator"></div>
    </div>

        </li>

       
        <li data-content="gallery">


    <div class="row">

        <?php
            foreach ($imagenes as $imagen) {
                
               echo "
               <div class='list-group gallery'>
                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                        <a class='thumbnail fancybox' rel='shadowbox' href='../$imagen->url'>
                            <div class='span3'>
                            <img class='img-responsive' width='320' height='320' src='/$imagen->url' />
                            </div>
                            <div class='text-right'>
                                <small class='text-muted'>$imagen->autor</small>
                            </div> <!-- text-right / end -->
                        </a>
                    </div> <!-- col-6 / end -->
                </div>
               ";

            }
        ?>
            
    </div> <!-- row / end -->

            
        </li>

    </ul> <!-- cd-tabs-content -->
</div> <!-- cd-tabs -->

    

</div>

 <script src="/js/jquery-1.11.1.min.js"></script>
<script src="/js/ion.rangeSlider.js"></script>

<script>

jQuery(function($) {
  $("#range").ionRangeSlider({
            hide_min_max: true,
            keyboard: true,
            min: 0,
            max: 4000,
            from: $("#min").val(),
            to: $("#max").val(),
            type: 'double',
            step: 1,
            postfix: "m.s.n.m",
            grid: true,
            grid_num: 10,
            disable: true
        });

  $("#tamano").ionRangeSlider({
            hide_min_max: true,
            keyboard: true,
            min: 0,
            max: 200,
            from: 0,
            to: $("#tamano").val(),
            type: 'double',
            step: 1,
            postfix: "m",
            grid: true,
            grid_num: 10,
            disable: true
        });
})

    
</script>

<script src="/js/jquery-2.1.1.js"></script>
<script src="/js/main.js"></script> <!-- Resource jQuery -->






@include('includes.footer')