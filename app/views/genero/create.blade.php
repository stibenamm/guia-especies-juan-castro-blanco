<!DOCTYPE HTML>
<html lang="es-ES">
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <div class="row">

            <!--errores de validación-->
            @if($errors->has())
                <div class="alert-box alert">          
                    @foreach ($errors->all('<p>:message</p>') as $message)
                        {{ $message }}
                    @endforeach
                    
                </div>
            @endif
 
            <table>
                {{ Form::open(array('url' => 'genero/create')) }}
                <tr>
                    <td>
                        {{ Form::label('nombre', 'Nombre') }}
                    </td>
                    <td>
                        {{ Form::text('nombre', Input::old('nombre')) }}
                    </td>
                </tr>
                 <tr>
                    <td>
                        {{ Form::label('familia_id', 'Familia ID') }}
                    </td>
                    <td>
                        {{ Form::text('familia_id', Input::old('familia_id')) }}
                    </td>
                </tr>
                <tr>
                    <td>
 
                    </td>
                    <td>
                         {{ Form::submit('Ingresar genero') }}
                    </td>
                </tr>              
                {{ Form::close() }}
            </table>    
            
        </div>
    </body>
</html>