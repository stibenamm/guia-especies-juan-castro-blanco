<div id="decorative2">
    <div class="container">

        <div class="divPanel topArea notop nobottom">
            <div class="row-fluid">
                <div class="span12">

                    <div id="divLogo" class="pull-left">
                        <a href="/" id="divSiteTitle">GUIA DIGITAL</a><br />
                        <a href="/" id="divTagLine">Especies Arbóreas</a>
                    </div>

                    <div id="divMenuRight" class="pull-right">
                    <div class="navbar">
                        <button type="button" class="btn btn-navbar-highlight btn-large btn-primary" data-toggle="collapse" data-target=".nav-collapse">
                            MENU <span class="icon-chevron-down icon-white"></span>
                        </button>
                        <div class="nav-collapse collapse">
                            <ul class="nav nav-pills ddmenu">
                                <li class="dropdown active"><a href="/">Inicio</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle">Clasificacion <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                            <li><a href="/familia/list">Familia</a></li>
                            <li><a href="/genero/list">Genero</a></li>
                            <li><a href="/especie/list">Especie</a></li>
                            </ul>
                                </li>
                                <li class="dropdown"><a href="#">Contactos</a></li>
                            </ul>
                        </div>
                    </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>