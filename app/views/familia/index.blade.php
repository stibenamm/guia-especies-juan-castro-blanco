<!DOCTYPE HTML>
<html lang="es-ES">
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <div class="row">
            {{ HTML::link(URL::to('familia/create'), 'Nueva familia') }}
             <ul>
            @if(count($familias) > 0)
          
                @foreach($familias as $familia)
 
                    <li>
                        Nombre: 
                        {{ $familia->nombre }}
                        {{ HTML::link(URL::to('familia/update/'.$familia->id), 'Actualizar familia') }}
                        {{ HTML::link(URL::to('familia/delete/'.$familia->id), 'Eliminar familia') }}
                    </li>
 
                @endforeach
            
            @endif  
            </ul>  
 
            <!--mostramos mensajes conforme pasen acontecimientos-->
            @if(Session::has('mensaje'))
                <div>
                    {{ Session::get('mensaje') }}
                </div>
            @endif
        </div>
    </body>
</html>