 @include('includes.navigation')

        <div class="container">

        <div class="row">

            <!--errores de validación-->
            @if($errors->has())
                <div class="alert-box alert">          
                    @foreach ($errors->all('<p>:message</p>') as $message)
                        {{ $message }}
                    @endforeach
                    
                </div>
            @endif
 
            <table>
                {{ Form::open(array('url' => 'imagen/create', 'files' => true)) }}
                <tr>
                    <td>
                        {{ Form::label('imagen', 'imagen') }}
                    </td>
                    <td>
                        {{ Form::file('imagen') }}
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ Form::label('especie_id', 'Especie ID') }}
                    </td>
                    <td>
                        {{ Form::text('especie_id', Input::old('especie_id')) }}
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ Form::label('autor', 'Autor') }}
                    </td>
                    <td>
                        {{ Form::text('autor', Input::old('autor')) }}
                    </td>
                </tr>
                <tr>
                    <td>
 
                    </td>
                    <td>
                         {{ Form::submit('Ingresar imagen') }}
                    </td>
                </tr>              
                {{ Form::close() }}
            </table>    
            
        </div>
</div>