 @include('includes.navigation')

 <div class="container">
        <div class="row">
            
            <table>
                {{ Form::open(array('url' => 'imagen/update/'.$imagen->id)) }}
                <tr>
                    <td>
                        {{ Form::label('especie_id', 'Especie ID') }}
                    </td>
                    <td>
                        {{ Form::text('especie_id', Input::old('especie_id') ? Input::old('especie_id') : $imagen->especie_id) }}
                    </td>
                </tr>
                <tr>
 
                    </td>
                    <td>
                         {{ Form::submit('Buscar imagenes') }}
                    </td>
                </tr>              
                {{ Form::close() }}
            </table>    
             <ul>
            @if(count($imagenes) > 0)
          
                @foreach($imagenes as $imagen)
 
                    <li>
                        URL: 
                        {{ $imagen->url }}
                        Especie ID: 
                        {{ $imagen->especie_id }}
                        Autor: 
                        {{ $imagen->autor }}
                        {{ HTML::link(URL::to('imagen/update/'.$imagen->id), 'Actualizar imagen') }}
                        {{ HTML::link(URL::to('imagen/delete/'.$imagen->id), 'Eliminar imagen') }}
                    </li>
 
                @endforeach
            
            @endif  
            </ul>  
 
            <!--mostramos mensajes conforme pasen acontecimientos-->
            @if(Session::has('mensaje'))
                <div>
                    {{ Session::get('mensaje') }}
                </div>
            @endif
        </div>
        </div>
