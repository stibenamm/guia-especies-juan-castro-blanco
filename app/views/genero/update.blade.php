<!DOCTYPE HTML>
<html lang="es-ES">
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <div class="row">
            <!--errores de validación-->
            @if($errors->has())
                <div class="alert-box alert">          
                    @foreach ($errors->all('<p>:message</p>') as $message)
                        {{ $message }}
                    @endforeach
                    
                </div>
            @endif
 
            <table>
                {{ Form::open(array('url' => 'genero/update/'.$genero->id)) }}
                <tr>
                    <td>
                        {{ Form::label('nombre', 'Nombre') }}
                    </td>
                    <td>
                        {{ Form::text('nombre', Input::old('nombre') ? Input::old('nombre') : $genero->nombre) }}
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ Form::label('familia_id', 'Familia') }}
                    </td>
                    <td>
                        {{ Form::text('familia_id', Input::old('familia_id') ? Input::old('familia_id') : $genero->familia->id) }}
                    </td>
                </tr>
                <tr>
                    <td>
 
                    </td>
                    <td>
                         {{ Form::submit('Actualizar genero') }}
                    </td>
                </tr>              
                {{ Form::close() }}
            </table>    
            
        </div>
    </body>
</html>