<!DOCTYPE HTML>
<html lang="es-ES">
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <div class="row">
            {{ HTML::link(URL::to('genero/create'), 'Ingresar genero') }}
             <ul>
            @if(count($generos) > 0)
          
                @foreach($generos as $genero)
 
                    <li>
                        Nombre: 
                        {{ $genero->nombre }}
                        Familia: 
                        {{ $genero->familia->nombre }}
                        {{ HTML::link(URL::to('genero/update/'.$genero->id), 'Actualizar genero') }}
                        {{ HTML::link(URL::to('genero/delete/'.$genero->id), 'Eliminar genero') }}
                    </li>
 
                @endforeach
            
            @endif  
            </ul>  
 
            <!--mostramos mensajes conforme pasen acontecimientos-->
            @if(Session::has('mensaje'))
                <div>
                    {{ Session::get('mensaje') }}
                </div>
            @endif
        </div>
    </body>
</html>