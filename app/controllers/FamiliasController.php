<?php

class FamiliasController extends \BaseController {

	
    public function index()
    {
 
        $familias = Familia::all();
        return View::make("familia.index", array("familias" => $familias));
 
    }


     public function list_all()
    {
        $familias = Familia::orderBy('nombre')->get();

        $this->layout->nest('content', 'familia.all', array("familias" => $familias)); 
    }
 
    public function create()
    {

    	//si es get
        if(Input::get())
        {
 
            $inputs = $this->getInputs(Input::all());
 
            if($this->validateForms($inputs) === true)
            {
 
                 $familia = new Familia($inputs);
 
                 if($familia->save())
                 {
 
                     return Redirect::to('familia/show')->with(array('mensaje' => 'El post ha sido creado correctamente.'));
 
                 }
 
            }else{
 
                return Redirect::to('familia/create')->withErrors($this->validateForms($inputs))->withInput();
 
            }
 
        //si es post
        }else{
 
            return View::make("familia.create");
 
        }
 
    }
 
    public function update($id)
    {
 
        $familia = Familia::find($id);
        if(Input::get())
        {
 
            if($familia)
            {
 
                $inputs = $this->getInputs(Input::all());
 
                if($this->validateForms($inputs) === true)
                {
 
                     $familia->nombre = Input::get("nombre");
 
                     if($familia->save())
                     {
 
                         return Redirect::to('familia/show')->with(array('mensaje' => 'La familia se ha actualizado correctamente.'));
 
                     }
 
                }else{
 
                    return Redirect::to("familia/update/$id")->withErrors($this->validateForms($inputs))->withInput();
 
                }
 
            }else{
 
                return Redirect::to('familia/show')->with(array('mensaje' => 'La familia no existe.'));
                
            }
 
        }else{
 
            return View::make("familia.update", array("familia" => $familia));
            
        }
 
    }
 
    public function delete($id)
    {
 
        $familia = Familia::find($id);
        if($familia)
        {
 
            $familia->delete();
            return Redirect::to('familia/show')->with(array('mensaje' => 'La familia ha sido eliminada correctamente.'));
 
        }else{
 
            return Redirect::to('familia/show')->with(array('mensaje' => "La familia que intentas eliminar no existe."));
 
        }
 
    }
 
    //método privado para validar los formularios
    //reutilización de código
    private function validateForms($inputs = array())
    {
 
        $rules = array(
            'nombre'      => 'required'
        );
            
        $messages = array(
            'required'  => 'El campo :attribute es obligatorio.'
        );
    
        $validation = Validator::make($inputs, $rules, $messages);
 
        if($validation->fails())
        {
 
            return $validation;
 
        }else{
 
            return true;
 
        }
 
    }
 
  
    private function getInputs($inputs = array())
    {
 
        foreach($inputs as $key => $val)
        {
            $inputs[$key] = $val;
        }
        return $inputs;
    }

}
			