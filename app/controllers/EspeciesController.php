<?php

class EspeciesController extends \BaseController {

    
    public function index()
    {
 
        $especies = Especie::all();
        return View::make("especie.index", array("especies" => $especies));
 
    }

    public function list_all()
    {
 
        $especies = Especie::orderBy('nombre_cientifico')->get();
        
        $this->layout->nest('content', 'especie.all', array("especies" => $especies)); 
    }

     public function show_by_id()
    {
        $id = Input::get("id");

        $especie = Especie::find($id);

        $imagenes = DB::select("select * from imagenes where especie_id = $id");

        $this->layout->nest('content', 'especie', array("especie" => $especie, "imagenes" => $imagenes)); 

    }

     public function search_especie()
    {
        

        $especies = DB::select("select e.id, e.nombre_comun, e.nombre_cientifico, g.nombre from especies e, generos g where e.genero_id = g.id;");


        return Response::json ($especies);
    }
 

     public function list_by_genero()
    {
        
        $genero_id = Input::get("id");

        $especies = DB::select("select * from especies where genero_id = $genero_id");


        $this->layout->nest('content', 'especie.all', array("especies" => $especies)); 
    }

    public function create()
    {

        //si es get
        if(Input::get())
        {
 
            $inputs = $this->getInputs(Input::all());
 
            if($this->validateForms($inputs) === true)
            {
 
                 $especie = new Especie($inputs);
 
                 if($especie->save())
                 {
 
                     return Redirect::to('especie/show')->with(array('mensaje' => 'La especie ha sido creada correctamente.'));
 
                 }
 
            }else{
 
                return Redirect::to('especie/create')->withErrors($this->validateForms($inputs))->withInput();
 
            }
 
        //si es post
        }else{
 
            return View::make("especie.create");
 
        }
 
    }
 
    public function update($id)
    {
 
        $especie = Especie::find($id);
        if(Input::get())
        {
 
            if($especie)
            {
 
                $inputs = $this->getInputs(Input::all());
 
                if($this->validateForms($inputs) === true)
                {
 
                     $especie->nombre_cientifico = Input::get("nombre_cientifico");
                     $especie->nombre_comun = Input::get("nombre_comun");
                     $especie->genero_id = Input::get("genero_id");
                     $especie->tipo = Input::get("tipo");
                     $especie->tamano = Input::get("tamano");
                     $especie->distribucion_minima = Input::get("distribucion_minima");
                     $especie->distribucion_maxima = Input::get("distribucion_maxima");
                     $especie->frutos_flores = Input::get("frutos_flores");
                     $especie->descripcion = Input::get("descripcion");
 
                     if($especie->save())
                     {
 
                         return Redirect::to('especie/show')->with(array('mensaje' => 'La especie se ha actualizado correctamente.'));
 
                     }
 
                }else{
 
                    return Redirect::to("especie/update/$id")->withErrors($this->validateForms($inputs))->withInput();
 
                }
 
            }else{
 
                return Redirect::to('especie/show')->with(array('mensaje' => 'La especie no existe.'));
                
            }
 
        }else{
 
            return View::make("especie.update", array("especie" => $especie));
            
        }
 
    }
 
    public function delete($id)
    {
 
        $especie = Especie::find($id);
        if($especie)
        {
 
            $especie->delete();
            return Redirect::to('especie/show')->with(array('mensaje' => 'La especie ha sido eliminado correctamente.'));
 
        }else{
 
            return Redirect::to('especie/show')->with(array('mensaje' => "La especie que intentas eliminar no existe."));
 
        }
 
    }
 
    //método privado para validar los formularios
    //reutilización de código
    private function validateForms($inputs = array())
    {
 
        $rules = array(
            'nombre_cientifico'      => 'required',
            'nombre_comun'      => 'required',
            'genero_id'      => 'required|numeric',
            'tipo'      => 'required',
            'tamano'      => 'required',
            'distribucion_minima'      => 'required|numeric',
            'distribucion_maxima'      => 'required|numeric',
            'frutos_flores'      => 'required',
            'descripcion'      => 'required'
        );

        $messages = array(
            'required'  => 'El campo :attribute es obligatorio.',
            'numeric'  => 'El campo :attribute es de tipo numero.'

        );
    
        $validation = Validator::make($inputs, $rules, $messages);
 
        if($validation->fails())
        {
 
            return $validation;
 
        }else{
 
            return true;
 
        }
 
    }
 
  
    private function getInputs($inputs = array())
    {
 
        foreach($inputs as $key => $val)
        {
            $inputs[$key] = $val;
        }
        return $inputs;
    }

}
            