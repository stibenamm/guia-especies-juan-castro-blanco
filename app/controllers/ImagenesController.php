<?php

class ImagenesController extends \BaseController {

	
    public function index()
    {
 
        $imagenes = Imagen::all();
        $this->layout->nest('content', 'imagen.index', array("imagenes" => $imagenes)); 
 
    }

    public function find()
    {
    
        $imagenes = Imagen::where('especie_id', Input::get('especie_id'))->first();
         $this->layout->nest('content', 'imagen.result', array("imagenes" => $imagenes)); 
 
    }

    
    public function upload(){

    }

    public function create()
    {
    	//si es get
        if(Input::get())
        {
 
            $inputs = $this->getInputs(Input::all());
            
            if($this->validateForms($inputs) === true)
            {
 
                $imagen = new Imagen();

                $file = Input::file('imagen');
                $destinationPath = 'public/uploads';
                $upload_success = Input::file('imagen')->move($destinationPath, $file->getClientOriginalName());


                 $imagen->url = 'uploads/' . $file->getClientOriginalName();
                 $imagen->especie_id = Input::get("especie_id");
                 $imagen->autor = Input::get("autor");
 
                 if($imagen->save())
                 {
 
                     return Redirect::to('imagen/show')->with(array('mensaje' => 'La imagen ha sido creada correctamente.'));
 
                 }
 
            }else{
 
                return Redirect::to('imagen/create')->withErrors($this->validateForms($inputs))->withInput();
 
            }
 
        //si es post
        }else{
 
             $this->layout->nest('content', 'imagen.create'); 
 
        }
 
    }
 
    public function update($id)
    {
 
        $imagen = Imagen::find($id);
        if(Input::get())
        {
 
            if($imagen)
            {
 
                $inputs = $this->getInputs(Input::all());
 
                if($this->validateForms($inputs) === true)
                {
                    
                    $file = Input::file('imagen');
                    $destinationPath = 'uploads';
                    $filename = str_random(12);
                    $upload_success = Input::file('imagen')->move($destinationPath, $filename);


                     $imagen->url = $destinationPath. $filename;
                     $imagen->especie_id = Input::get("especie_id");
                     $imagen->autor = Input::get("autor");
 
                     if($imagen->save())
                     {
 
                         return Redirect::to('imagen/show')->with(array('mensaje' => 'La imagen se ha actualizado correctamente.'));
 
                     }
 
                }else{
 
                    return Redirect::to("imagen/update/$id")->withErrors($this->validateForms($inputs))->withInput();
 
                }
 
            }else{
 
                return Redirect::to('imagen/show')->with(array('mensaje' => 'La imagen no existe.'));
                
            }
 
        }else{
 
             $this->layout->nest('content', 'imagen.update', array("imagen" => $imagen)); 
            
        }
 
    }
 
    public function delete($id)
    {
 
        $imagen = Imagen::find($id);
        if($imagen)
        {
 
            $imagen->delete();
            return Redirect::to('imagen/show')->with(array('mensaje' => 'La imagen ha sido eliminada correctamente.'));
 
        }else{
 
            return Redirect::to('imagen/show')->with(array('mensaje' => "La imagen que intentas eliminar no existe."));
 
        }
 
    }
 
    //método privado para validar los formularios
    //reutilización de código
    private function validateForms($inputs = array())
    {
 
        $rules = array(
            //'url'      => '',
            'especie_id'      => 'required|numeric',
            'autor'      => 'required',
        );
            
        $messages = array(
            'required'  => 'El campo :attribute es obligatorio.'
        );
    
        $validation = Validator::make($inputs, $rules, $messages);
 
        if($validation->fails())
        {
 
            return $validation;
 
        }else{
 
            return true;
 
        }
 
    }
 
  
    private function getInputs($inputs = array())
    {
 
        foreach($inputs as $key => $val)
        {
            $inputs[$key] = $val;
        }
        return $inputs;
    }

}
			