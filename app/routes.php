<?php

Route::get('/', 'HomeController@index');

Route::get('especie/show_by_id', 'EspeciesController@show_by_id');
Route::get('especie/search_especie', 'EspeciesController@search_especie');
Route::get('genero/list', 'GenerosController@list_all');
Route::get('genero/list_by_familia', 'GenerosController@list_by_familia');
Route::get('especie/list', 'EspeciesController@list_all');
Route::get('especie/list_by_genero', 'EspeciesController@list_by_genero');
Route::get('familia/list', 'FamiliasController@list_all');


Route::get('familia/show', 'FamiliasController@index');
Route::get('genero/show', 'GenerosController@index');
Route::get('especie/show', 'EspeciesController@index');
Route::get('imagen/show', 'ImagenesController@index');

//rutas privadas
Route::group(array('before' => 'auth.basic'), function () {

 //familia

 Route::get('familia/create', 'FamiliasController@create');
 Route::get('familia/update/{id}', 'FamiliasController@update');
 Route::get('familia/delete/{id}', 'FamiliasController@delete');
 Route::post('familia/create', 'FamiliasController@create');
 Route::post('familia/update/{id}', 'FamiliasController@update');

 //genero

 Route::get('genero/create', 'GenerosController@create');
 Route::get('genero/update/{id}', 'GenerosController@update');
 Route::get('genero/delete/{id}', 'GenerosController@delete');
 Route::post('genero/create', 'GenerosController@create');
 Route::post('genero/update/{id}', 'GenerosController@update');

 //especie

 Route::get('especie/create', 'EspeciesController@create');
 Route::get('especie/update/{id}', 'EspeciesController@update');
 Route::get('especie/delete/{id}', 'EspeciesController@delete');
 Route::post('especie/create', 'EspeciesController@create');
 Route::post('especie/update/{id}', 'EspeciesController@update');

 //imagen
 Route::get('imagen/find', 'ImagenesController@find');
 Route::get('imagen/create', 'ImagenesController@create');
 Route::get('imagen/update/{id}', 'ImagenesController@update');
 Route::get('imagen/delete/{id}', 'ImagenesController@delete');
 Route::post('imagen/create', 'ImagenesController@create');
 Route::post('imagen/update/{id}', 'ImagenesController@update');

});

Route::get('/agregarUsuario', function()
{

	User::create(array(
		    'username' => 'admin',
		    'password' => Hash::make('admin')
	));

        echo "agregado";

});
