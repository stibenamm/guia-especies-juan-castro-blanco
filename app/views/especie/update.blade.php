<!DOCTYPE HTML>
<html lang="es-ES">
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <div class="row">
            <!--errores de validación-->
            @if($errors->has())
                <div class="alert-box alert">          
                    @foreach ($errors->all('<p>:message</p>') as $message)
                        {{ $message }}
                    @endforeach
                    
                </div>
            @endif
 
            <table>
                {{ Form::open(array('url' => 'especie/update/'.$especie->id)) }}
                <tr>
                    <td>
                        {{ Form::label('nombre_cientifico', 'Nombre Cientifico') }}
                    </td>
                    <td>
                        {{ Form::text('nombre_cientifico', Input::old('nombre_cientifico') ? Input::old('nombre_cientifico') : $especie->nombre_cientifico) }}
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ Form::label('nombre_comun', 'Nombre Común') }}
                    </td>
                    <td>
                        {{ Form::text('nombre_comun', Input::old('nombre_comun') ? Input::old('nombre_comun') : $especie->nombre_comun) }}
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ Form::label('genero_id', 'Genero ID') }}
                    </td>
                    <td>
                        {{ Form::text('genero_id', Input::old('genero_id') ? Input::old('genero_id') : $especie->genero_id) }}
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ Form::label('tipo', 'Tipo') }}
                    </td>
                    <td>
                        {{ Form::text('tipo', Input::old('tipo') ? Input::old('tipo') : $especie->tipo) }}
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ Form::label('tamano', 'Tamaño') }}
                    </td>
                    <td>
                        {{ Form::text('tamano', Input::old('tamano') ? Input::old('tamano') : $especie->tamano) }}
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ Form::label('distribucion_minima', 'Distribución minima') }}
                    </td>
                    <td>
                        {{ Form::text('distribucion_minima', Input::old('distribucion_minima') ? Input::old('distribucion_minima') : $especie->distribucion_minima) }}
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ Form::label('distribucion_maxima', 'Distribución maxima') }}
                    </td>
                    <td>
                        {{ Form::text('distribucion_maxima', Input::old('distribucion_maxima') ? Input::old('distribucion_maxima') : $especie->distribucion_maxima) }}
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ Form::label('frutos_flores', 'Frutos y flores') }}
                    </td>
                    <td>
                        {{ Form::text('frutos_flores', Input::old('frutos_flores') ? Input::old('frutos_flores') : $especie->frutos_flores) }}
                    </td>
                </tr>
                <tr>
                    <td>
                        {{ Form::label('descripcion', 'Descripción') }}
                    </td>
                    <td>
                        {{ Form::text('descripcion', Input::old('descripcion') ? Input::old('descripcion') : $especie->descripcion) }}
                    </td>
                </tr>
                <tr>
                    <td>
 
                    </td>
                    <td>
                         {{ Form::submit('Actualizar especie') }}
                    </td>
                </tr>              
                {{ Form::close() }}
            </table>    
            
        </div>
    </body>
</html>