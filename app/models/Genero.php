<?php

	class Genero extends Eloquent
	{
		protected $table = 'generos';
		protected $fillable = array("nombre","familia_id");

		public function especies(){
	 		return $this->hasMany('Especie');
	 	}

	 	public function familia(){
	 		return $this->belongsTo('Familia');
	 	}
	}