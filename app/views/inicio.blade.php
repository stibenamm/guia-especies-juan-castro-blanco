@include('includes.navigation')
<div id="decorative1" style="position:relative">
    <div class="container">

        <div class="divPanel headerArea">
            <div class="row-fluid">
                <div class="span12">

                        <div id="headerSeparator"></div>

                        <div id="divHeaderText" class="page-content">
                            <div id="divHeaderLine2">Parque Nacional del Agua Juan Castro Blanco</div><br />
                            <div id="divHeaderLine3"><a class="btn btn-large btn-primary" href="#">Mas Informacion</a></div>
                        </div>


                </div>
            </div>

        </div>

    </div>
</div>

<div id="contentOuterSeparator"></div>

<div class="container">

    <div class="divPanel page-content">

        <div class="row-fluid">

                <div class="span12" id="divMain">

                    <h1>Bienvenidos</h1>

                    <p> Esta guía esta hecha manera didáctica
						y simplificada para ayudar a usuarios sin conocimientos a reconocer y distinguir las
						principales especies de árboles existentes en el Parque Nacional del Agua Juan Castro Blanco, esto con el apoyo de Coopelesca,
						Universidad Tecnica Nacional y APANAJUCA.

                    </p>

                    <hr style="margin:45px 0 35px" />
                    

                    <div class="row-fluid">
                        <div class="span8 offset2">

                            <div class="sidebox">
                                <h3 class="sidebox-title">Busqueda de especies</h3>
                                <p>
                                    <div class="input-append">
                                        <input class="span10" id="autocomplete" size="16" type="text"><button class="btn" id="buscar" type="button">Buscar</button>
                                    </div>
                                </p>                      
                            </div>
                                                                                       
                        </div>

                    </div>

                    <hr style="margin:45px 0 35px" />
                    <br />

                    <div class="list_carousel responsive">
                        <ul id="list_photos">
                            <li><img src="img/roble.jpg" class="img-polaroid">  </li>
                            <li><img src="img/cedro.jpg" class="img-polaroid">  </li>
                            <li><img src="img/guarumo.jpg" class="img-polaroid">  </li>
                            <li><img src="img/aguacatillo.jpg" class="img-polaroid">  </li>
							<li><img src="img/a1.jpg" class="img-polaroid">  </li>                            
                            <li><img src="img/a2.jpg" class="img-polaroid">  </li>
                            <li><img src="img/a3.jpg" class="img-polaroid">  </li>
                            <li><img src="img/a4.jpg" class="img-polaroid">  </li>
							<li><img src="img/a5.jpg" class="img-polaroid">  </li>
							<li><img src="img/a6.jpg" class="img-polaroid">  </li>
                        </ul>
                    </div> 
					
                </div>

            </div>

        <div id="footerInnerSeparator"></div>
    </div>

</div>




@include('includes.footer')