<?php

	class Especie extends Eloquent
	{
		protected $table = 'especies';
		protected $fillable = array("nombre_cientifico","nombre_comun","genero_id","tipo","tamano","distribucion_minima","distribucion_maxima","frutos_flores","descripcion");

		public function imagenes(){
	 		return $this->hasMany('Imagen');
	 	}

	 	public function genero(){
	 		return $this->belongsTo('Genero');
	 	}

	}

	