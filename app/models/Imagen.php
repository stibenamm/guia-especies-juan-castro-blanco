<?php

	class Imagen extends Eloquent
	{
		protected $table = 'imagenes';
		protected $fillable = array("url","especie_id,autor");

	 	public function especie(){
	 		return $this->belongsTo('Especie');
	 	}
	}