<!DOCTYPE HTML>
<html lang="es-ES">
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <div class="row">
            {{ HTML::link(URL::to('especie/create'), 'Ingresar especie') }}
             <ul>
            @if(count($especies) > 0)
          
                @foreach($especies as $especie)
 
                    <li>
                        Nombre Cientifico: 
                        {{ $especie->nombre_cientifico }}
                        Nombre Común: 
                        {{ $especie->nombre_comun }}
                        Genero ID: 
                        {{ $especie->genero_id }}
                        Tipo: 
                        {{ $especie->tipo }}
                        Tamaño: 
                        {{ $especie->tamano }}
                        Distribución minima: 
                        {{ $especie->distribucion_minima }}
                         Distribución maxima: 
                        {{ $especie->distribucion_maxima }}
                         Frutos y flores: 
                        {{ $especie->frutos_flores }}
                        Descripción: 
                        {{ $especie->descripcion }}
                        {{ HTML::link(URL::to('especie/update/'.$especie->id), 'Actualizar especie') }}
                        {{ HTML::link(URL::to('especie/delete/'.$especie->id), 'Eliminar especie') }}
                    </li>
 
                @endforeach
            
            @endif  
            </ul>  
 
            <!--mostramos mensajes conforme pasen acontecimientos-->
            @if(Session::has('mensaje'))
                <div>
                    {{ Session::get('mensaje') }}
                </div>
            @endif
        </div>
    </body>
</html>