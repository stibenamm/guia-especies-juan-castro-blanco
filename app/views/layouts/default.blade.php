<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Guia Digital de Especies Arbóreas</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content=""> 

      <link rel="stylesheet" href="/css/reset.css"> <!-- CSS reset -->
  <link rel="stylesheet" href="/css/style.css"> <!-- Resource style -->
  <script src="/js/modernizr.js"></script> <!-- Modernizr -->
  
  {{HTML::style('js/bootstrap/css/bootstrap.min.css');}}
  {{HTML::style('js/bootstrap/css/bootstrap-responsive.min.css');}}
  

  {{HTML::style('js/icons/general/stylesheets/general_foundicons.css');}}
  {{HTML::style('js/icons/social/stylesheets/social_foundicons.css');}}
  {{HTML::style('js/fontawesome/css/font-awesome.min.css');}}

  {{HTML::style('js/carousel/style.css');}}
  {{HTML::style('http://fonts.googleapis.com/css?family=Source+Sans+Pro');}}
  {{HTML::style('http://fonts.googleapis.com/css?family=Open+Sans');}}
  {{HTML::style('http://fonts.googleapis.com/css?family=Abel');}}
  {{HTML::style('http://fonts.googleapis.com/css?family=Open+Sans');}}
  

  <link rel="stylesheet" href="/css/normalize.css" />
    <link rel="stylesheet" href="/css/ion.rangeSlider.css" />
    <link rel="stylesheet" href="/css/ion.rangeSlider.skinFlat.css" />

    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css" href="/css/shadowbox.css">
<script type="text/javascript" src="/js/shadowbox.js"></script>
<script type="text/javascript">
 Shadowbox.init();
</script>

{{HTML::style('css/custom.css');}}


</head>
<body>

  {{ $content }}

  {{HTML::script('js/jquery.min.js');}}
  {{HTML::script('js/jquery.autocomplete.min.js');}}
  {{HTML::script('js/bootstrap/js/bootstrap.min.js');}}
 

<script src="/js/carousel/jquery.carouFredSel-6.2.0-packed.js" type="text/javascript"></script><script type="text/javascript">$('#list_photos').carouFredSel({ responsive: true, width: '100%', scroll: 2, items: {width: 320,visible: {min: 2, max: 6}} });</script>
  {{HTML::script('js/default.js');}}

<script type="text/javascript">
  
$(function(){
  
  var especies = [];
    $.ajax({
          url: '/especie/search_especie',
          data: { text : $("#text").val()}
        })
        .done(function(response) {
             
            for (var i = 0; i < response.length; i++) {

              especies.push(
                {
                  value: response[i].nombre + " " + response[i].nombre_cientifico,
                  data: response[i].id
                }
              );

              especies.push(
                {
                  value: response[i].nombre_comun,
                  data: response[i].id
                }
              );
            };

        
        })
        .fail(function() {
          alert('error');
        });


  // setup autocomplete function pulling from currencies[] array
  $('#autocomplete').autocomplete({
    lookup: especies,
    onSelect: function (suggestion) {
        window.location.href = "/especie/show_by_id?id="+suggestion.data;
    }
  });
  

});

</script>

</body>
</html>